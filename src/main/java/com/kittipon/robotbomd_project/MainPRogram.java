/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.robotbomd_project;

import java.util.Scanner;

/**
 *
 * @author kitti
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Tablemap map = new Tablemap(10, 10);
        Robot robot = new Robot(2, 2, 'X', map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {
            map.Showmap();
            char direction = inputDirection(kb);
            if (direction == 'q') {
                printBye();
                break;
            }
            robot.walk(direction);
        }

    }

    private static void printBye() {
        System.out.println("Okay Goodbye :)");
    }

    private static char inputDirection(Scanner kb) {
        String input = kb.next();
        char direction = input.charAt(0);
        showDirection(direction);
        return direction;
    }

    private static void showDirection(char direction) {
        if (direction != 'q') {
            System.out.println();
            System.out.println("Your direction is " + direction);
        }
    }

}
