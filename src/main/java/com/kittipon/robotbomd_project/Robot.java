/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.robotbomd_project;

/**
 *
 * @author kitti
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private Tablemap map;

    public Robot(int x, int y, char symbol, Tablemap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'W':
            case 'w':
                walkNorth();
                break;
            case 'S':
            case 's':
                walkSouth();
                break;
            case 'D':
            case 'd':
                walkEast();
                break;
            case 'A':
            case 'a':
                walkWest();
                break;
        }
        showFoundbomb();
        return true;
    }

    private void showFoundbomb() {
        if (map.isBomb(x, y)) {
            System.out.println("Found Bomb!!!"+"(" + x +", "+y+")");
        }
    }

    private void walkWest() {
        if (map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            showOutofmap();
        }
    }

    private void walkEast() {
        if (map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            showOutofmap();
        }
    }

    private void walkSouth() {
        if (map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            showOutofmap();
        }
    }

    private void walkNorth() {
        if (map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            showOutofmap();
        }
    }

    private void showOutofmap() {
        System.out.print("You are walk out of map");
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
