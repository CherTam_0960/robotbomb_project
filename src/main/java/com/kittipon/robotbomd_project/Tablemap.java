/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.robotbomd_project;

/**
 *
 * @author kitti
 */
public class Tablemap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public Tablemap(int width, int heigh) {
        this.width = width;
        this.height = heigh;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void Showmap() {
        System.out.println("Map here!!!");
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (robot.isOn(x, y)) {
                    showRobot();
                } else if (bomb.isOn(x, y)) {
                    showBomb();
                } else {
                    showDash();
                }
            }
            newLine();
        }
    }

    private void newLine() {
        System.out.println("");
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showDash() {
        System.out.print("-");
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.getX() == x && bomb.getY() == y;
    }
}
